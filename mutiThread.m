//
//  UMeng.m
//  Gallery
//
//  Created by marshluca on 11-2-10.
//  Copyright 2011 www.5yi.com. All rights reserved.
//
 
#import "UMeng.h"
#import "MBase.h"
 
@implementation UMeng
 
- (id)initWithTask:(SEL)task Data:(NSMutableDictionary *)data
{
	if (self = [super init]) {		
	}
	
	selector = task;
	dict = [[NSMutableDictionary alloc] initWithDictionary:data];	
	
	if (!MAppDelegate.queue) {
		MAppDelegate.queue = [[NSOperationQueue alloc] init];
	}	
	
	return [self autorelease];
}
 
- (void) process 
{
	NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self 
																			selector:selector
																			  object:nil];
	[MAppDelegate.queue addOperation:operation];
	[operation release];
}
 
 
- (void)appLaunched
{
	[MobClick setDelegate:self];
	[MobClick appLaunched];
}
 
- (void)appTerminated
{
	[MobClick setDelegate:self];
	[MobClick appTerminated];
}
 
 
- (void)handleEvent
{
	NSString *event_id = [dict objectForKey:@"event_id"];
	NSString *label = [dict objectForKey:@"label"];
	
	if (label) {
		[MobClick event:event_id label:label];
	} else {
		[MobClick event:event_id];
	}
}
 
 
#pragma mark  MobClickDelegate methods
- (NSString *)appKey{
	return @"4d533cab112cf76a6601007a";
}
 
- (NSString *)channelId{
	return @"App Store";
}
 
 
- (void)dealloc 
{
	[dict release];
	[super dealloc];
}
 
@end